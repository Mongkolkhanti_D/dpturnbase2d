﻿using UnityEngine;

namespace Assets.Scriptss
{
    public class AnimCon : MonoBehaviour
    {
        [SerializeField]
        private Animator _animCamera, _animCardSelect, _animPlayer, _animOppo;

        public void FullScreen()
        {
            _animCamera.Play("zoomplayer");
            _animCardSelect.Play("AppCard");
        }

        public void CardSelectedScreen()
        {
            _animCamera.Play("zoomoutplayer");
            _animCardSelect.Play("DisappCard");
        }

        public void PlayAttackCard()
        {
            _animPlayer.Play("playerAttack");
        }
        public void PlayDefendCard()
        {
            _animPlayer.Play("playerDefend");
        }
        public void PlayHealCard()
        {
            _animPlayer.Play("playerHeal");
        }

        public void PlayerDead()
        {
            _animPlayer.Play("playerDead");
        }

        public void OppoAttack()
        {
            _animOppo.Play("oppoAttack");
        }

        public void OppoDefend()
        {
            _animOppo.Play("oppodefend");
        }

        public void OppoDead()
        {
            _animOppo.Play("oppoDead");
        }
    }
}
