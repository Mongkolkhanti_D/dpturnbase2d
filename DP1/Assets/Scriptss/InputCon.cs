﻿using UnityEngine;

namespace Assets.Scriptss
{
    public class InputCon : MonoBehaviour
    {
        private AnimCon _animCon;
        private GamePlayCon _gameCon;

        void Awake()
        {
            _animCon = GetComponent<AnimCon>();
            _gameCon = GetComponent<GamePlayCon>();
        }

        public void SelectCard()
        {
            string _cardType = UnityEngine.EventSystems.
                               EventSystem.current.
                               currentSelectedGameObject.name;
            CardSelect _cardSelect = CardSelect.NONE;
            switch (_cardType)
            {
                case "AttackCard":
                    _cardSelect = CardSelect.ATTACK;
                    break;
                case "DefendCard":
                    _cardSelect = CardSelect.DEFEND;
                    break;
                case "HealCard":
                    _cardSelect = CardSelect.HEAL;
                    break;
            }

            _gameCon.PlayCard(_cardSelect);
            _animCon.CardSelectedScreen();
        }
    }
}
