﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scriptss
{
    public class HPplayerbar : MonoBehaviour
    {
        public Image _hpfill;
        public float _maxHP;
        public float _currentHP;
        public float _smoothTime = 0.07f;
        public Gradient _fillColor;

        private AnimCon _animCon;

        void Awake()
        {
            _animCon = GetComponent<AnimCon>();
        }

        public void PlayerTakeDamage(float damage)
        {
            _currentHP -= damage;
        }

        private void Update()
        {
            _hpfill.fillAmount = Mathf.Lerp(_hpfill.fillAmount, getHpFillAmount(), _smoothTime);
            _hpfill.color = _fillColor.Evaluate(_hpfill.fillAmount);

            if(_currentHP <= 0)
            {
                _animCon.PlayerDead();
                NextScenes();
            }
        }

        float getHpFillAmount()
        {
            return _currentHP * (1 / _maxHP);
        }

        void NextScenes()
        {
            SceneManager.LoadScene("Lose");
        }

    }
}
