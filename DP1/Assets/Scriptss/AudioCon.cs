﻿using UnityEngine;

namespace Assets.Scriptss
{
    public class AudioCon : MonoBehaviour
    {
        public AudioSource _audioSource;
        public AudioClip playerAtack, playerpowerUp;

        public void PlayerAttackSound()
        {
            _audioSource.clip = playerAtack;
            _audioSource.Play();
        }
        public void PlayerUpSound()
        {
            _audioSource.clip = playerpowerUp;
            _audioSource.Play();
        }
    }
}
