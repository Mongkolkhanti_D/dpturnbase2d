﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scriptss
{
    public enum CardSelect
    {
        NONE,
        ATTACK,
        DEFEND,
        HEAL
    }

    public class GamePlayCon : MonoBehaviour
    {
        private AnimCon _animCon;
        private HPplayerbar _hpPlayer;
        private HPoppobar _hpOppo;
        private AudioCon _audioCon;
        private CardSelect _Card = CardSelect.NONE, _opCard = CardSelect.NONE;

        void Awake()
        {
            _animCon = GetComponent<AnimCon>();
            _hpPlayer = GetComponent<HPplayerbar>();
            _hpOppo = GetComponent<HPoppobar>();
            _audioCon = GetComponent<AudioCon>();
        }
        public void PlayCard(CardSelect _playCard)
        {
            switch (_playCard)
            {
                case CardSelect.ATTACK:
                    _animCon.PlayAttackCard();
                    _audioCon.PlayerAttackSound();
                    _Card = CardSelect.ATTACK;
                    break;
                case CardSelect.DEFEND:
                    _animCon.PlayDefendCard();
                    _audioCon.PlayerUpSound();
                    _Card = CardSelect.DEFEND;
                    break;
                case CardSelect.HEAL:
                    _animCon.PlayHealCard();
                    _audioCon.PlayerUpSound();
                    _Card = CardSelect.HEAL;
                    break;
            }

            OpponentCard();
            DetermineWinner();
        }

        void OpponentCard()
        {
            int rnd = Random.Range(0, 2);

            switch (rnd)
            {
                case 0:
                    _opCard = CardSelect.ATTACK;
                    _animCon.OppoAttack();
                    break;
                case 1:
                    _opCard = CardSelect.DEFEND;
                    _animCon.OppoDefend();
                    break;
            }
        }

        void DetermineWinner()
        {
            if(_Card == _opCard)
            {
                _hpPlayer.PlayerTakeDamage(1);
                _hpOppo.OppoTakeDamage(1);
            }
            if (_Card == CardSelect.ATTACK&&_opCard == CardSelect.DEFEND)
            {
                _hpPlayer.PlayerTakeDamage(2);
            }
            if (_Card == CardSelect.DEFEND && _opCard == CardSelect.ATTACK)
            {
                _hpOppo.OppoTakeDamage(2);
            }
            if (_Card == CardSelect.HEAL && _opCard == CardSelect.ATTACK)
            {
                _hpPlayer.PlayerTakeDamage(-1);
                _hpOppo.OppoTakeDamage(2);
            }
            if (_Card == CardSelect.HEAL && _opCard == CardSelect.DEFEND)
            {
                _hpPlayer.PlayerTakeDamage(-1);
            }
        }
    }
}
