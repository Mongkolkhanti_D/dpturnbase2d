﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scriptss
{
    public class TimelineSceneCon : MonoBehaviour
    {
        public float _changTime;

        private void Update()
        {
            _changTime -= Time.deltaTime;
            if(_changTime <= 0)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
}
